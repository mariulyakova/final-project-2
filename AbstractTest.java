import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class AbstractTest {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String username;
    private static String password;
    private static String base_url;
    private static String token;

    @BeforeAll
    static void initTest() throws IOException {

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username = prop.getProperty("username");
        password = prop.getProperty("password");
        base_url = prop.getProperty("base_url");
        token = prop.getProperty("token");

    }

    public static String getUsername() {
        return username;
    }

    public static String getPassword(){
        return password;
    }

    public static String getBase_url(){
        return base_url;
    }
    public static String getToken(){
        return token;
    }






}
